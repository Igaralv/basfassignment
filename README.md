Instructions:

-To execute the functional and API tests just go to the root directory of the project and execute "mvn clean test"
-Chrome version 88.0.4324.182 needs to be installed in the system.
-Performance tests are not yet integrated in the Maven cycle, please find a Jmeter script under "src/test/jmeter"